package ru.t1.nikitushkina.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.nikitushkina.web.enumerated.Status;
import ru.t1.nikitushkina.web.model.Project;
import ru.t1.nikitushkina.web.repository.ProjectRepository;

import java.time.LocalDate;

@Controller
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/project/create")
    public String create() {
        projectRepository.save(new Project("Test Project " + LocalDate.now().toString()));
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @ModelAttribute("project") Project project,
            BindingResult result
    ) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Project project = projectRepository.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project",project);
        modelAndView.addObject("status",getStatuses());
        return modelAndView;
    }

    private Status[] getStatuses() {
        return Status.values();
    }

}
