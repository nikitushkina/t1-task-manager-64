package ru.t1.nikitushkina.web.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.nikitushkina.web")
public class ApplicationConfiguration {
}
