<html>
<head>
    <title>TASK MANAGER</title>
</head>
<style>
    h1 {
        font-size: 1.6em;
    }

    a {
        color: indianred;
    }

    select {
        width: 150px;
    }
</style>

<body>
<table width="100%" height="100%" border="1" style="border: black">
    <tr>
        <td height="35" width="150" nowrap="nowrap" align="center">
            <a href="http://localhost:8080/">TASK MANAGER</a>
        </td>
        <td width="100%" align="right">
            <a href="/projects">PROJECTS</a>

            <a href="/tasks">TASKS</a>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="100%" valign="top" style="border: black">
